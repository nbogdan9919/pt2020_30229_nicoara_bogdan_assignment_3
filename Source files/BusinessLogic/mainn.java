package BusinessLogic;
import java.io.File;

/**
 * clasa principala, De aici se citeste fisierul de intrare
 */
public class mainn {



    public static void main(String[] args) throws Exception
    {

        if(args.length!=1)
        {
            System.out.println("Format gresit, inserati un singur argument(fisierul de intrare");
           System. exit(1);
        }
        String intrare=args[0];
        File in=new File(intrare);

        CmdManagement cmd=new CmdManagement(in);
        cmd.manageCmd(in);

    }
}
