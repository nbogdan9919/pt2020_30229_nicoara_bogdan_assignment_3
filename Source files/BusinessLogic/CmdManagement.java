package BusinessLogic;
import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSmartCopy;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Logger;
import java.util.List;
import java.util.*;
import java.util.logging.*;
import java.sql.Connection;
import java.sql.*;
import DataAccess.*;
import Model.*;
import Presentation.*;

/**
 * Aici ne ocupam de fisierul de intrare, aici se face parsarea fisierului de intrare,
 * se apeleaza metodele pentru inserarea in tabele,pentru stergere, pentru update,
 * se apeleaza metodele pt generarea pdf-urilor
 */
public class CmdManagement {

    private File intrare;
    private GeneratePDF pdf;
    private DBOperations op;
    private int nrBills=0;
    private int nrPdfClienti=0;
    private int nrPdfOrders=0;
    private int nrPdfProduse=0;

    public CmdManagement(File intrare)
    {
        this.intrare=intrare;
    }

    /**
     * Transforma o linie de intrare intr-un client cu nume si Adresa
     * @param s reprezinta o linie din fisierul de intrare
     * @return un client cu un nume si o adresa
     */
    public Client stringToClient(String s)
    {
        Client cl=new Client();
        List<String> regex=new LinkedList<>(Arrays.asList(s.split("Insert client|,|:|Delete client")));
        regex.removeAll(Arrays.asList("",null));          //fara apar caractere empty in lista din cauza regex ca are limit 0

        cl.setNume(regex.get(0));
        cl.setAdresa(regex.get(1));

        return cl;
    }

    /**
     * Transforma o linie de intrare intr-un produs cu nume ,cantitate si pret
     * @param s reprezinta o linie din fisierul de intrare
     * @return un produs cu nume cantitate si pret
     */
    public Produs stringToProdus(String s)
    {
        Produs pr=new Produs();

        List<String> regex=new LinkedList<>(Arrays.asList(s.split("Insert product|,|:|Delete Product")));
        regex.removeAll(Arrays.asList("",null));

        pr.setNume(regex.get(0));
        pr.setCantitate(Float.parseFloat(regex.get(1)));
        pr.setPret(Float.parseFloat(regex.get(2)));

        return pr;
    }

    /**
     * Transforma o linie de intrare intr-un order cu numele Clientului,a produsului si cantiatea produsului
     * @param s reprezinta o linie din fisierul de intrare
     * @return un order cu nume client,produs si cantitate produs
     */
    public Order stringToOrder(String s)
    {
        Order ord=new Order();

        List<String> regex=new LinkedList<>(Arrays.asList(s.split("Order|,|:")));
        regex.removeAll(Arrays.asList("",null));

        ord.setClient(regex.get(0));
        ord.setProdus(regex.get(1));
        ord.setCantitate(Float.parseFloat(regex.get(2)));

        return ord;
    }

    private FileOutputStream a;

    /**
     * Aici ne ocupam de comenziile de tip "Report", care presupun generarea de PDF-uri
     * @param line reprezinta o linie din fisierul de intrare
     */
    public void manageReport(String line)
    {
        if(line.contains("Report client"))
        {   nrPdfClienti++;
            pdf.pdfClient(nrPdfClienti);
        }

        if(line.contains("Report product")) {
            nrPdfProduse++;
            pdf.pdfProdus(nrPdfProduse);
        }
        if(line.contains("Report order")) {
            nrPdfOrders++;
            pdf.pdfOrder(nrPdfOrders);
        }
    }

    /**
     * Aici ne ocupam de comenziile de tip "Insert", acestea se transforma din string in  obiecte de tip Model
     * si sunt trimise mai departe pentru a fi inserate in tabele
     * @param line reprezinta o linie din fisierul de intrare
     */
    public void manageInsert(String line)
    {
        if(line.contains("Insert client"))
        {
            Client a=stringToClient(line);
            op.insertClient(a);
        }
        if(line.contains("Insert product"))
        {
            Produs a=stringToProdus(line);
            op.insertProdus(a);
        }

    }

    /**
     * Aici ne ocupam de comenziile de tip "Order"
     * generam facturi, si se insereaza Order in tabel
     * @param line reprezinta o linie din fisierul de intrare
     */
    public void manageOrder(String line)
    {
        if(line.contains("Order"))
        {
            nrBills++;
            Order a=stringToOrder(line);
            op.insertFactura(a);

            if(DBQueries.checkOrderPossible(a)==1)
                pdf.PdfBill(nrBills);
            else
                pdf.invalidBill(nrBills);

            op.insertOrder(a);
        }
    }

    /**
     * Aici se analizeaza liniile din fisier, se verifica ce fel de tip de comanda este
     * Report, Insert, Order, Delete Product, Delete Client si se trimit mai departe
     * la metodele prezentate mai sus
     * @param line reprezinta o linie din fisier
     *
     */
    public void manageLine(String line) throws Exception
    {

        List<String> comenzi=Arrays.asList(line.split("\n"));

        for(String cuvant:comenzi)
        {
            if(cuvant.contains("Report"))
                manageReport(cuvant);

            if(cuvant.contains("Insert"))
                manageInsert(cuvant);

            if(cuvant.contains("Order"))
                manageOrder(cuvant);

            if(cuvant.contains("Delete Product"))
            {    cuvant=cuvant+", 0 ,"+"0";
                Produs a=stringToProdus(cuvant);
                op.deleteProduct(a);
            }
            if(cuvant.contains("Delete client"))
            {
                cuvant=cuvant+", ";
                Client a=stringToClient(cuvant);
                op.deleteClient(a);
            }
        }
    }

    /**
     * Aici se RESETEAZA tabelele si PDF-URILE GENERATE ANTERIOR
     * si se parseaza fisierul de intrare in linii care sunt trimise
     * catre manageLine
     * @param file reprezinta fisierul de intrare
     *
     */
    public void manageCmd(File file) throws Exception
    {
        DBOperations.reset();
        GeneratePDF.resetFiles();

        try{
            Scanner s1=new Scanner(file);

            while(s1.hasNext())
            {
                List <String> linii=Arrays.asList(s1.nextLine());
                for(String linie : linii)
                {
                    manageLine(linie);
                }

            }
        }catch(FileNotFoundException e)
        {

        }
    }

}
