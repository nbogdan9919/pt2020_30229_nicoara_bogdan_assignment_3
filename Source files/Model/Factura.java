package Model;

/**
 * Clasa pentru factura, se tin: nume client, nume produs, cantitate produs, pret produs
 * in rest doar gettere si settere.
 */
public class Factura {

    private String client;
    private String produs;
    private float cantitate;
    private float pret;

    public Factura()
    {

    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getProdus() {
        return produs;
    }

    public void setProdus(String produs) {
        this.produs = produs;
    }

    public float getCantitate() {
        return cantitate;
    }

    public void setCantitate(float cantitate) {
        this.cantitate = cantitate;
    }

    public float getPret() {
        return pret;
    }

    public void setPret(float pret) {
        this.pret = pret;
    }


    public String toString()
    {
        String s="";
        s=s+client+" "+produs+" cantitate "+cantitate+" pret "+pret;

        return s;
    }
}
