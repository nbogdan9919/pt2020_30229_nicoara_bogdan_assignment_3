package Model;

/**
 * clasa pentru Order, Se retin nume client, nume produs, id client, id produs, cantitate produs
 * in rest doar gettere si settere.
 */
public class Order {


    private String Client;
    private String Produs;
    private float cantitate;
    private int idClient;
    private int idProdus;

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public int getIdProdus() {
        return idProdus;
    }

    public void setIdProdus(int idProdus) {
        this.idProdus = idProdus;
    }

    public Order()
    {

    }

    public Order(String client,String Produs,float cantitate)
    {
        this.Client=client;
        this.Produs=Produs;
        this.cantitate=cantitate;
    }

    public String getClient() {
        return Client;
    }

    public void setClient(String client) {
        Client = client;
    }

    public String getProdus() {
        return Produs;
    }

    public void setProdus(String produs) {
        Produs = produs;
    }

    public float getCantitate() {
        return cantitate;
    }

    public void setCantitate(float cantitate) {
        this.cantitate = cantitate;
    }

    public String toString()
    {
        String s="";
        s=s+" "+Client+" "+Produs+" "+cantitate;

        return s;
    }
}
