package Model;

/**
 *Clasa Produs, se retin: id produslui, numele, cantiatea si pretul
 * in rest doar gettere si settere.
 */
public class Produs {

    private int idProdus;
    private String nume;
    private float cantitate;
    private float pret;

    public Produs()
    {

    }
    public Produs(int id,String nume,float cantitate,float pret)
    {
        this.idProdus=id;
        this.nume=nume;
        this.cantitate=cantitate;
        this.pret=pret;

    }

    public int getIdProdus() {
        return idProdus;
    }

    public void setIdProdus(int idProdus) {
        this.idProdus = idProdus;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public float getCantitate() {
        return cantitate;
    }

    public void setCantitate(float cantitate) {
        this.cantitate = cantitate;
    }

    public float getPret(){return pret;}

    public void setPret(float pret){this.pret=pret;}

    public String toString()
    {
        String s="";
        s=s+idProdus+" "+nume+" "+cantitate+" "+pret;

        return s;
    }

}
