package Model;

/**
 *Clasa pentru clienti se tin: idClient, nume Client, adresa clientului
 * in rest doar gettere si settere.
 */
public class Client {

    private int idClient;
    private String nume;
    private String adresa;

    public Client()
    {

    }

    public Client(int id,String nume,String prenume,String adresa)
    {
        this.idClient=id;
        this.nume=nume;
        this.adresa=adresa;

    }

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int id) {
        this.idClient = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }


    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public String toString()
    {
        String s="";
        s=s+idClient+" "+nume+" "+adresa;

        return s;
    }
}
