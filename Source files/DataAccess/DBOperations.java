package DataAccess;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import Model.*;

/**
 * Clasa pentru operatiile specifice operatiilor: insert/delete
 * Se face update in caz de nevoie pentru unele tabele
 */
public class DBOperations {

    public DBOperations()
    {

    }

    /**
     * Se genereaza un id pentru client
     *Se insereaza un client in baza de date
     */
    public static void insertClient(Client cl)
    {
        Connection connection=ConnectDB.getConnection();
        int id= DBQueries.createClientId(cl);
        PreparedStatement ps;
        String query;
        cl.setIdClient(id);

        try {
            query = "insert into client (id, nume, adresa)" + "values (?, ?, ?)";

            ps = connection.prepareStatement(query);
            ps.setString(1,String.valueOf(cl.getIdClient()));
            ps.setString(2,cl.getNume());
            ps.setString(3,cl.getAdresa());
            ps.execute();
        }catch(Exception e){}
    }

    /**
     *Se insereaza un produs care nu exista deja in baza de date
     */
    public static void insertProdusFirstTime(Produs pr)
    {
        Connection connection=ConnectDB.getConnection();
        PreparedStatement ps;

        try {
            String query = "insert into produs (id, nume, pret, cantitate)" + "values (?, ?, ?, ?)";

            ps = connection.prepareStatement(query);
            ps.setString(1, String.valueOf(pr.getIdProdus()));
            ps.setString(2, pr.getNume());
            ps.setString(3, String.valueOf(pr.getPret()));
            ps.setString(4, String.valueOf(pr.getCantitate()));

            ps.execute();
        } catch (Exception e) { }

    }

    /**
     * In cazul in care un produs exista deja
     * prin aceasta metoda I se va face update la cantiate
     */
    public static void updateCantitateProdus(Produs pr)
    {
        Connection connection=ConnectDB.getConnection();
        PreparedStatement ps;

        try{
            String query="update produs set cantitate=? where nume=?";
            ps=connection.prepareStatement(query);
            ps.setString(1,String.valueOf(pr.getCantitate()+ DBQueries.checkCantitateProdus(pr)));
            ps.setString(2,pr.getNume());
            ps.execute();
        }catch(Exception e){}

    }

    /**
     * Se genereaza un id pentru produs
     * Se verifica daca un produs exista in baza de date
     * si se va insera sau se va face update dupa caz
     */
    public static void insertProdus(Produs pr)
    {
        int id= DBQueries.createProdusId(pr);
        pr.setIdProdus(id+1);

        if(DBQueries.checkCantitateProdus(pr)==-1) {

            insertProdusFirstTime(pr);

        }
        else{

            updateCantitateProdus(pr);

        }
    }

    /**
     *Se insereaza in tabela Orders o comanda valida
     */
    public static void insertOrderValid(Order ord)
    {
        Connection connection=ConnectDB.getConnection();
        PreparedStatement ps;

        try {
            String query = "insert into orders (client, produs, cantitate,idClient,idProdus)" + "values (?, ?, ?, ?, ?)";

            ps = connection.prepareStatement(query);
            ps.setString(1, ord.getClient());
            ps.setString(2, ord.getProdus());
            ps.setString(3, String.valueOf(ord.getCantitate()));
            ps.setString(4,String.valueOf(ord.getIdClient()));
            ps.setString(5,String.valueOf(ord.getIdProdus()));

            ps.execute();
        } catch (Exception e) {
        }
    }

    /**
     * Se genereaza id-uri pentru order in functie de clienti/produse
     *Se verifica daca comanda e posibila si se face update la produse in caz pozitiv
     */
    public static void insertOrder(Order ord)
    {
        Connection connection=ConnectDB.getConnection();
        PreparedStatement ps;

        ord= DBQueries.OrderIds(ord);

        if(DBQueries.checkOrderPossible(ord)==1) {

            insertOrderValid(ord);
            Produs pr=new Produs();
            pr.setNume(ord.getProdus());
            pr.setCantitate(-1*ord.getCantitate());
            updateCantitateProdus(pr);
        }
    }

    /**
     * Se creeaza o noua factura,se calculeaza din order cantitate,nume client,nume produs
     * si se calculeaza pretul pentru factura
     * Dupa care se sterge din tabela fill ultima factura si se insereaza aceasta pentru ca
     * bill este gandit sa tina o singura factura( se face comanda dupa se sterge factura)
     */
    public static  void insertFactura(Order ord)
    {
        Factura a=new Factura();
        a.setClient(ord.getClient());
        a.setProdus(ord.getProdus());
        a.setCantitate(ord.getCantitate());
        Produs prd=new Produs();
        prd.setNume(ord.getProdus());
        a.setPret(DBQueries.pretPentruOrder(prd)*ord.getCantitate());

        Connection connection=ConnectDB.getConnection();
        PreparedStatement ps;

        try{
            ps=connection.prepareStatement("Delete from bill");
            ps.execute();

            String query = "insert into bill (client, produs, cantitate,pret)" + "values (?, ?, ?, ?)";

            ps = connection.prepareStatement(query);
            ps.setString(1, a.getClient());
            ps.setString(2, a.getProdus());
            ps.setString(3, String.valueOf(a.getCantitate()));
            ps.setString(4,String.valueOf(a.getPret()));

            ps.execute();
        } catch (Exception e) { }
    }

    /**
     *Stergerea unui produs atat din tabela orders cat si din tabela produs
     * in functie de numele produsului
     */
    public static void deleteProduct(Produs prod)
    {
        Connection connection=ConnectDB.getConnection();
        PreparedStatement ps;

        String query="Delete from produs where nume='"+prod.getNume()+"'";

        try{
            ps=connection.prepareStatement("Delete from orders where produs='"+prod.getNume()+"'");
            ps.execute();

            ps=connection.prepareStatement(query);
            ps.execute();

        }
        catch(Exception e){}
    }

    /**
     *Se sterge un client/mai multi clienti din baza de date
     * in cazul in care s-a oferit o adresa pt un client, se va sterge clientul cu numele si adresa data
     * in caz contrat se vor sterge toti clientii cu numele primit, indiferent de adresa
     */
    public static void deleteClient(Client cl)
    {
        Connection connection=ConnectDB.getConnection();
        PreparedStatement ps;
        String query,query2;
        cl.setIdClient(DBQueries.getIdClient(cl));

        if(cl.getAdresa().length()==1)
        {
            query="Delete from client where nume='"+cl.getNume()+"'";
            query2="Delete from orders where client='"+cl.getNume()+"'";
        }
        else {
            query = "Delete from client where nume='"+cl.getNume()+"' and adresa='"+cl.getAdresa()+"'";
            query2="Delete from orders where idClient='"+cl.getIdClient()+"'";
        }

        try{
            ps=connection.prepareStatement(query2);
            ps.execute();

            ps=connection.prepareStatement(query);
            ps.execute();

        }catch(Exception e){}

    }

    /**
     * Metoda pentru a reseta tabelele dupa fiecare rulare a fisierului de intrare
     */
    public static void reset()
    {
        Connection connection=ConnectDB.getConnection();
        PreparedStatement ps;

        try{
            ps=connection.prepareStatement("Delete from client");
            ps.execute();
            ps=connection.prepareStatement("Delete from produs");
            ps.execute();
            ps=connection.prepareStatement("Delete from orders");
            ps.execute();


        }catch(Exception e){}

    }

}
