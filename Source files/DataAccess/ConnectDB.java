package DataAccess;

import java.util.logging.Logger;
import java.util.*;
import java.util.logging.*;
import java.sql.Connection;
import java.sql.*;
import java.sql.DriverManager;
import Model.*;


/**
 * Aici se realizeaza conexiunea la baza de date
 */
public class ConnectDB {

    private static final Logger a=Logger.getLogger(ConnectDB.class.getName());
    private static final String DRIVER="com.mysql.cj.jdbc.Driver";
    private static final String DBURL="jdbc:mysql://localhost:3309/tema3";
    private static final String USER="root";
    private static final String PASS="root";

    private static ConnectDB singleInstance=new ConnectDB();
    private static Connection connection;


    private ConnectDB() {
        try {
            Class.forName(DRIVER);
            connection=createConnection();
        } catch (ClassNotFoundException e) {

        }
    }

    private Connection createConnection()
    {
        Connection connection=null;
        try{
            connection=DriverManager.getConnection(DBURL,USER,PASS);
        }
        catch(SQLException e){

        }
        return connection;
    }

    /**
     * Metoda pentru a obtine conexiunea la baza de date
     * @return conexiunea catre baza de date
     */
    public static Connection getConnection()
    {
        try {
            if(connection==null) {
                Class.forName(DRIVER);
                connection = DriverManager.getConnection(DBURL, USER, PASS);
            }
        }
        catch(Exception e)
        {

        }
        return connection;
    }




    public static void main(String[] args)
    {
        Connection connection=null;
        connection= ConnectDB.getConnection();

        if(connection!=null)
        {
            System.out.println("ok");
        }

        PreparedStatement ps;
        int value=21;

    }
}
