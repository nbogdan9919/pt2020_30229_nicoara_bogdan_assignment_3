package DataAccess;

import Model.*;

import java.sql.ResultSet;

/**
 * Clasa in care vom transforma liniile returnate de un Query in obiecte de tip Model
 */
public class QueryToModel {

    /**
     * Transforma o linie returnata de Query intr-un Client
     * @param result rezultatul unui query(o linie)
     * @return un obiect de tip Client
     */
    public static Client queryToClient(ResultSet result)
    {
        Client a=new Client();

        try {
            a.setIdClient(result.getInt("id"));
            a.setNume(result.getString("nume"));
            a.setAdresa(result.getString("adresa"));
        }catch(Exception e)
        {
        }
        return a;
    }

    /**
     * Transforma o linie returnata de Query intr-un Produs
     * @param result rezultatul unui query(o linie)
     * @return un obiect de tip Produs
     */
    public static Produs queryToProdus(ResultSet result)
    {
        Produs a=new Produs();

        try {
            a.setIdProdus(result.getInt("id"));
            a.setNume(result.getString("nume"));
            a.setPret(result.getFloat("pret"));
            a.setCantitate(result.getFloat("cantitate"));
        }catch(Exception e)
        {
        }
        return a;
    }

    /**
     * Transforma o linie returnata de Query intr-un Order
     * @param result rezultatul unui query(o linie)
     * @return un obiect de tip Order
     */
    public static Order queryToOrder(ResultSet result)
    {
        Order a=new Order();

        try {
            a.setClient(result.getString("client"));
            a.setProdus(result.getString("produs"));
            a.setCantitate(result.getFloat("cantitate"));
        }catch(Exception e)
        {
        }

        return a;
    }

    /**
     * Transforma o linie returnata de Query intr-un Bill
     * @param result rezultatul unui query(o linie)
     * @return un obiect de tip Bill
     */
    public static Factura queryToBill(ResultSet result)
    {
        Factura a=new Factura();

        try {
            a.setClient(result.getString("client"));
            a.setProdus(result.getString("produs"));
            a.setCantitate(result.getFloat("cantitate"));
            a.setPret(result.getFloat("pret"));
        }catch(Exception e)
        {
        }

        return a;
    }

}
