package DataAccess;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import Model.*;

/**
 * Clasa in care se calculeaza diferite lucruri din tabele de care avem nevoie in celelalte clase
 */
public class DBQueries {

    /**
     * Metoda pentru generarea unui id pentru un client citit din fisier
     * @param cl reprezinta un client care are nume si adresa(din fisierul de intrare)
     * @return un id unic pentru acest client
     */
    public static int createClientId(Client cl)
    {
        Connection connection=ConnectDB.getConnection();
        int id=0;
        PreparedStatement ps;
        String query="select MAX(id) as id from `client`";
        try {
            ps = connection.prepareStatement(query);
            ResultSet rez=ps.executeQuery();
            while(rez.next()) {

                if (id < rez.getInt("id")) ;
                {
                    id = rez.getInt("id");
                }
            }
        }catch(Exception e){}
        return id+1;
    }

    /**
     * Metoda pentru generarea unui id unic pentru un produs
     * @param pr reprezinta un produs cu datele citite din fisier
     * @return un id unic pentru produs
     */
    public static int createProdusId(Produs pr)
    {
        Connection connection=ConnectDB.getConnection();
        int id=0;
        PreparedStatement ps;
        String query="select MAX(id) as id from `produs`";
        try {
            ps = connection.prepareStatement(query);
            ResultSet rez=ps.executeQuery();
            while(rez.next()) {

                if (id < rez.getInt("id")) ;
                {
                    id = rez.getInt("id");
                }
            }
        }catch(Exception e){}
        return id+1;
    }

    /**
     * Metoda pt a verifica ce cantitate din produsul cautat mai exista in stoc(tabel)
     * @param pr se primeste numele unui produs
     * @return cantitatea curenta a produsului(din tabel)
     */
    public static float checkCantitateProdus(Produs pr)
    {
        Connection connection=ConnectDB.getConnection();
        PreparedStatement ps;
        String query="Select * from produs where nume='"+pr.getNume()+"'";
        float cantitate=-1;

        try{
            ps=connection.prepareStatement(query);
            ResultSet rez=ps.executeQuery();
            while(rez.next())
            {
                if(cantitate==-1)
                    cantitate=0;
                cantitate=cantitate+rez.getFloat("cantitate");
            }
        }
        catch(Exception e){}

        return cantitate;
    }

    /**
     * Metoda pentru a verifica daca o comanda este realizabila sau nu
     * @param ord o comanda
     * @return daca comanda este posibila sau nu
     */


    public static int checkOrderPossible(Order ord)
    {
        Produs pr=new Produs();
        pr.setNume(ord.getProdus());

        if(DBQueries.checkCantitateProdus(pr)>=ord.getCantitate())
        {
            return 1;
        }
        return 0;
    }

    /**
     * Metoda pentru a obtine adresa unui client, avand comanda cu numele acestuia
     * @param ord o comanda
     * @return un client cu adresa generata folosing numele acestuia din Order
     */
    public static Client adresaClientFromOrder(Order ord)
    {
        Client cl=new Client();
        Connection connection=ConnectDB.getConnection();
        PreparedStatement ps;

        try {
            ps = connection.prepareStatement("select * from client where nume='" + ord.getClient() + "'");
            ResultSet rez = ps.executeQuery();

            while (rez.next()) {
                cl.setAdresa(rez.getString("adresa"));
            }
        }catch(Exception e){}

        return cl;

    }

    /**
     * Seteaza id-urile pentru client si produs in order
     * @param ord un order citit din fisier(cu nume client, nume produs)
     * @return un order cu id-urile pentru client si produs setate
     */
    public static Order OrderIds(Order ord)
    {
        Connection connection=ConnectDB.getConnection();
        PreparedStatement ps;
        Client cl=adresaClientFromOrder(ord);

        try{
            ps=connection.prepareStatement("select * from produs where nume='"+ord.getProdus()+"'");
            ResultSet rez=ps.executeQuery();

            while(rez.next())
            {
                ord.setIdProdus(rez.getInt("id"));
            }

            ps=connection.prepareStatement("select * from client where nume='"+ord.getClient()+"' and adresa='"+cl.getAdresa()+"'");
            rez=ps.executeQuery();

            while(rez.next())
            {
                ord.setIdClient(rez.getInt("id"));
            }
        }catch(Exception e){}

        return ord;
    }

    /**
     * Metoda pentru returnarea id-ului unui client din fisier folosing numele si adresa
     * @param cl reprezinta un client
     * @return id acestuia
     */

    public static int getIdClient(Client cl)
    {
        Connection connection=ConnectDB.getConnection();
        PreparedStatement ps;


        try{

            ps=connection.prepareStatement("select * from client where nume='"+cl.getNume()+"' and adresa='"+cl.getAdresa()+"'");
            ResultSet rez=ps.executeQuery();
            rez=ps.executeQuery();

            while(rez.next())
            {
                cl.setIdClient(rez.getInt("id"));
            }
        }catch(Exception e){}

        return cl.getIdClient();
    }

    /**
     * Metoda pentru a obtine pretul pentru un produs(avand numele acestuia)
     * @param prd reprezinta un produs
     * @return pretul unui produs
     */
    public static float pretPentruOrder(Produs prd)
    {
        float pret=0;
        Connection connection=ConnectDB.getConnection();
        PreparedStatement ps;

        String query="Select pret from produs where nume='"+prd.getNume()+"'";
        try {

            ps=connection.prepareStatement(query);
            ResultSet rez=ps.executeQuery();
            while (rez.next()) {
                pret = rez.getFloat("pret");

            }
        }catch(Exception e){}

        return pret;
    }


}
