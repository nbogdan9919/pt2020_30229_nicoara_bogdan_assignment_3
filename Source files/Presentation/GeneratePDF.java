package Presentation;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import Model.*;
import DataAccess.*;

/**
 * Clasa in care se genereaza toate pdf-urile si se RESETEAZA toate pdf existente in fisier
 */
public class GeneratePDF {

    /**
     * Aceasta metoda ia calea curenta in care se afla fisierul
     * si sterge toate fisierele cu extensia .pdf din fisier
     */
    public static void resetFiles()
    {
        Path currentPath= Paths.get("");
        String dir=currentPath.toAbsolutePath().toString();

        File file=new File(dir);

        File flist[]=file.listFiles();

        for(int i=0;i<flist.length;i++)
        {
            String f=flist[i].toString();
            if(f.endsWith(".pdf"))
            {
                boolean deleted=flist[i].delete();
            }

        }

    }

    /**
     *adauga in tabel parametrii specifici unui client
     */
    private static void addRows(PdfPTable tabel,Client a)
    {
        tabel.addCell(String.valueOf(a.getIdClient()));
        tabel.addCell(a.getNume());
        tabel.addCell(a.getAdresa());

    }

    /**
     *genereaza Pdf cu tabelul Clienti
     * @param nr reprezinta al catelea pdf este
     */
    public static void pdfClient(int nr){

        Connection connection=ConnectDB.getConnection();
        Client a;
        String query="select * from `client`";
        PdfPTable tabel=new PdfPTable(3);
        tabel.addCell("id");tabel.addCell("nume");tabel.addCell("adresa");

        Document document=new Document();
        try{
            PdfWriter.getInstance(document,new FileOutputStream("Clienti"+nr+".pdf"));

            document.open();

        }
        catch(Exception e){}

        try {
            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet result = ps.executeQuery();

            while (result.next()) {
                a=QueryToModel.queryToClient(result);
                addRows(tabel,a);
            }
            document.add(tabel);
        }
        catch(Exception e){}

        document.close();
    }

    /**
     * adauga in tabel parametrii specifici unui produs
     */
    private static void addRows(PdfPTable tabel,Produs a)
    {
        tabel.addCell(String.valueOf(a.getIdProdus()));
        tabel.addCell(a.getNume());
        tabel.addCell(String.valueOf(a.getPret()));
        tabel.addCell(String.valueOf(a.getCantitate()));

    }

    /**
     * Se genereaza un pdf cu tabelul Produs
     * @param nr reprezinta al catelea pdf este
     */
    public static void pdfProdus(int nr)
    {
        Connection connection=ConnectDB.getConnection();
        Produs a;
        String query="select * from `produs`";
        PdfPTable tabel=new PdfPTable(4);
        tabel.addCell("id");tabel.addCell("nume");tabel.addCell("pret");tabel.addCell("cantitate");

        Document document=new Document();
        try{

            PdfWriter.getInstance(document,new FileOutputStream("Produse"+nr+".pdf"));
            document.open();
        }
        catch(Exception e){}

        try {
            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet result = ps.executeQuery();

            while (result.next()) {

                a=QueryToModel.queryToProdus(result);
                addRows(tabel,a);
            }
            document.add(tabel);
        }
        catch(Exception e){}



        document.close();

    }

    /**
     * adauga in tabel parametrii specifici unui order
     */
    public static void addRows(PdfPTable tabel,Order a)
    {
        tabel.addCell(a.getClient());
        tabel.addCell((a.getProdus()));
        tabel.addCell(String.valueOf(a.getCantitate()));


    }

    /**
     *genereaza Pdf cu tabelul Order
     * @param nr reprezinta al catelea pdf este
     */
    public static void pdfOrder(int nr)
    {
        Connection connection=ConnectDB.getConnection();
        Order a;
        String query="select * from `orders`";
        PdfPTable tabel=new PdfPTable(3);

        tabel.addCell("Client");tabel.addCell("Produs");tabel.addCell("cantitate");
        PdfReader copy;

        Document document=new Document();

        try{
            PdfWriter.getInstance(document,new FileOutputStream("Orders"+nr+".pdf"));
            document.open();
        }
        catch(Exception e){}

        try {
            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet result = ps.executeQuery();

            while (result.next()) {

                a=QueryToModel.queryToOrder(result);
                addRows(tabel,a);
            }
            document.add(tabel);
            document.close();

        }
        catch(Exception e){}

    }

    /**
     *genereaza Pdf cu o factura
     * @param number reprezinta al catelea pdf este(a cata factura)
     */
    public static void PdfBill(int number)
    {
        Connection connection=ConnectDB.getConnection();
        Factura a;
        String query="select * from `bill`";
        Document document=new Document();

        try{
            PdfWriter.getInstance(document,new FileOutputStream("Bill"+number+".pdf"));
            document.open();
        }
        catch(Exception e){}

        try {
            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet result = ps.executeQuery();

            while (result.next()) {

                a = QueryToModel.queryToBill(result);
                document.add(new Paragraph("nume :" + a.getClient()));
                document.add(new Paragraph("Produs:" + a.getProdus()));
                document.add(new Paragraph("Cantitate:" + String.valueOf(a.getCantitate())));
                document.add(new Paragraph("Pret: " + String.valueOf(a.getPret())));
            }

            document.close();
        }
        catch(Exception e){}
    }

    /**
     *Se genereaza un pdf pentru o factura invalida
     * @param number reprezinta a cata factura este
     */
    public static void invalidBill(int number)
    {
        Document document=new Document();

        try{
            PdfWriter.getInstance(document,new FileOutputStream("Bill"+number+".pdf"));
            document.open();
        }
        catch(Exception e){}

        try {
            document.add(new Paragraph("Aceasta comanda nu este posibila"));
            document.close();
        }
        catch(Exception e){}


    }

}
